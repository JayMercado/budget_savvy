import React, { useContext } from 'react';
// Import nextJS Link component for client-side navigation
import Link from 'next/link';
// Import necessary bootstrap components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NavBar() {

	// Consume the UserContext and destructure it to access the user state from the context provider
	const { user } = useContext(UserContext);

	return (
		<Navbar style={{backgroundColor: "white !important"}} bg="light" expand="lg">
			<Link href="/">
				<a className="navbar-brand"><img src="/images/savvy_logo.png" alt="Budget Savvy" style={{width: "100px"}}/></a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					{(user.id !== null)
						?
						<React.Fragment>
							<Link href="/profile">
								<a className="nav-link" role="button">
									Profile
	                                </a>
							</Link>
							<Link href="/analytics">
								<a className="nav-link" role="button">
									Analytics
	                            </a>
							</Link>

						</React.Fragment>
						:
						null
					}
				</Nav>
				<Nav>
					{(user.id !== null)
						?
						<Link href="/logout">
							<a className="nav-link" role="button">
								Logout
	                        </a>
						</Link>
						:
						<React.Fragment>
							<Link href="/login">
								<a className="nav-link" role="button">
									Login
	                                </a>
							</Link>
							<Link href="/register">
								<a className="nav-link" role="button">
									Register
	                                </a>
							</Link>
						</React.Fragment>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}