import { useRouter } from 'next/router';
// Bootstrap components
import { Row, Col, Jumbotron } from 'react-bootstrap';
// Import nextJS Link component for client-side navigation


export default function Banner({data}) {

    const router = useRouter()
    // Destructure the data prop by its properties
    const {title, content} = data

    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1 className="text-center" >{title}</h1>
                    <p>{content}</p>
                </Jumbotron>
            </Col>
        </Row>
    )
}