import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import Router from 'next/router';
import UserContext from '../UserContext';
import View from '../components/View';
import AppHelper from '../app-helper';


export default function Login() {

	return (
		<View title={'Login'}>
			<Row className="justify-content-center">
				<Col xs md="6">
					<h3>Login</h3>
					<LoginForm />
				</Col>
			</Row>
		</View>
	)
}

const LoginForm = () => {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function authenticate(e) {

		//prevent redirection via form submission
		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/login`, options)
			.then(res => res.json())
			.then(data => {
				if (typeof data.accessToken !== 'undefined') {
					localStorage.setItem('token', data.accessToken)
					retrieveUserDetails(data.accessToken)
					Router.push('/profile')
				} else {
					if (data.error === 'does-not-exist') {
						Swal.fire('Authentication Failed', 'User does not exist.', 'error')
					} else if (data.error === 'login-type-error') {
						Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try an alternative login procedure.', 'error')
					} else if (data.error === 'incorrect-password') {
						Swal.fire('Authentication Failed', 'Password is incorrect.', 'error');
					}
				}

			})
	}

	const authenticateGoogleToken = (response) => {
		const payload = {

			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ tokenId: response.tokenId })

		}

		fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/verify-google-id-token`, payload)
			.then(res => res.json())
			.then(data => {
				if (typeof data.accessToken !== 'undefined') {
					localStorage.setItem('token', data.accessToken)
					retrieveUserDetails(data.accessToken)
					Router.push('/profile')
				} else {
					if (data.error === 'google-auth-error') {
						Swal.fire(
							'Google Auth Error',
							'Google authentication procedure failed',
							'error'
						)
					} else if (data.error === 'login-type-error') {
						Swal.fire(
							'Login Type Error',
							'You may have registered through a different login procredure.',
							'error'
						)
					}
				}
			})
	}

	const retrieveUserDetails = (accessToken) => {

		const options = {
			headers: { Authorization: `Bearer ${accessToken}` }
		};

		// localhost:4000/api/users/details
		fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, options)
			.then(res => res.json())
			.then(data => {
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});
			})

	}

	useEffect(() => {
		if ((email !== '' && password !== '')) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	return (
		<React.Fragment>
			<Card>
				<Card.Header>Login Details</Card.Header>
				<Card.Body>
					<Form onSubmit={authenticate}>
						<Form.Group controlId="userEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control
								type="email"
								placeholder="Enter email"
								value={email}
								onChange={(e) => setEmail(e.target.value)}
								autoComplete="off"
								required
							/>
						</Form.Group>

						<Form.Group controlId="password">
							<Form.Label>Password</Form.Label>
							<Form.Control
								type="password"
								placeholder="Password"
								value={password}
								onChange={(e) => setPassword(e.target.value)}
								required
							/>
						</Form.Group>

						{isActive
							?
							<div className="text-center">
								<Button variant="outline-primary" className="w-100" type="submit">
									Login
						</Button>
							</div>
							:
							<div className="text-center">
								<Button variant="outline-danger" className="w-100" type="submit" disabled>
									Login
						</Button>
							</div>
						}

						<GoogleLogin
							clientId="773839384895-ca72a1u0lbqslhd24c2g6trekkolp9dh.apps.googleusercontent.com"
							buttonText="Login"
							onSuccess={authenticateGoogleToken}
							onFailure={authenticateGoogleToken}
							cookiePolicy={'single_host_origin'}
							className="w-100 text-center d-flex justify-content-center my-3"
						/>

					</Form>
				</Card.Body>
			</Card>
		</React.Fragment>
	)
}