import React, { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import moment from 'moment';
import { Radar } from 'react-chartjs-2';
import { Form, Button, Row, Col } from 'react-bootstrap';
import randomColor from 'randomcolor';


export default function RadarChartExpense() {

    // States for dates
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [isActive, setIsActive] = useState(false);

    // State to store the transactions details of the user
    const [expenseLabels, setExpenseLabels] = useState([]);
    const [expenses, setExpenses] = useState([]);
    const [expenseBgColors, setExpenseBgColors] = useState([]);

    // Change format of date from form
    const firstDay = moment(startDate).format();
    const lastDay = moment(endDate).format();

    function filterByDate(e) {

        e.preventDefault()

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                /*
                *
                Filter Expense Records
                *
                */

                // Variable to temporarily store expense records
                const newExpenseRecords = [];

                // Loop through all records
                data.records.map(record => {
                    // Push only if record type is expense
                    if (record.type === "expense") {
                        newExpenseRecords.push(record)
                    }
                })

                // Variables to temporarily store filtered expense records
                let filteredExpenseRecords = [];

                if (newExpenseRecords.length > 0) {
                    // Loop through all expense records
                    newExpenseRecords.map(newExpenseRecord => {

                        // Change format of each expense date to match date format of the firstDay and lastDay
                        const newExpenseDay = moment(newExpenseRecord.date).format();

                        // Check if newExpenseDay is between firstday and lastDay
                        if (moment(newExpenseDay).isSameOrAfter(firstDay, "day") && moment(newExpenseDay).isSameOrBefore(lastDay, "day")) {
                            // Push Expense Data
                            filteredExpenseRecords.push(newExpenseRecord)
                        }
                    })
                }

                // Variable to temporarily store the distinct expense categories
                let filteredExpenseCategories = [];
                // Loop through filtered expense records
                filteredExpenseRecords.forEach(filteredExpenseRecord => {
                    // Filter duplicates expense categories
                    if (!filteredExpenseCategories.find(filteredExpenseCategory => filteredExpenseCategory === filteredExpenseRecord.category)) {
                        // Push inside temporary array only the distinct category
                        filteredExpenseCategories.push(filteredExpenseRecord.category);
                    }
                })

                // Variables to temporarily store expense data
                let filteredExpenseColors = [];
                let filteredExpenseAmounts = [];

                // Iterate over the distinct expense categories
                filteredExpenseCategories.map(filteredExpenseCategory => {
                    // Variable to temporarily store total amount of each expense category
                    let filteredExpenseAmountByCategory = 0;

                    // Iterate over every record in the raw JSON response from the API
                    filteredExpenseRecords.forEach(filteredExpenseRecord => {
                        if (filteredExpenseRecord.category === filteredExpenseCategory) {
                            filteredExpenseAmountByCategory = filteredExpenseAmountByCategory + parseInt(filteredExpenseRecord.amount)
                        }
                    })
                    filteredExpenseAmounts.push(filteredExpenseAmountByCategory)
                    filteredExpenseColors.push(randomColor())
                })
                setExpenseLabels(filteredExpenseCategories)
                setExpenses(filteredExpenseAmounts)
                setExpenseBgColors(filteredExpenseColors);

            })
    }

    // Validate form input whenever date fields are populated
    useEffect(() => {

        if (startDate && endDate) {
            setIsActive(true)
        }

    }, [startDate, endDate])

    useEffect(() => {
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data._id) {
                    /*
                    *
                    Expense Records Data
                    *
                    */

                    // Variable to temporarily store expense records
                    const expenseRecords = [];

                    // Loop through all records
                    data.records.map(record => {
                        // Push only if record type is expense
                        if (record.type === "expense") {
                            expenseRecords.push(record)
                        }
                    })

                    // Variable to temporarily store the distinct expense categories
                    let expenseCategories = [];
                    // Loop through all expense records
                    expenseRecords.forEach(expenseRecord => {
                        // Filter duplicates expense categories
                        if (!expenseCategories.find(expenseCategory => expenseCategory === expenseRecord.category)) {
                            // Push inside temporary array only the distinct category
                            expenseCategories.push(expenseRecord.category);
                        }
                    })

                    // Variables to temporarily store expense data
                    let expenseColors = [];
                    let expenseAmounts = [];

                    // Iterate over the distinct expense categories
                    expenseCategories.map(expenseCategory => {
                        // Variable to temporarily store total amount of each expense category
                        let expenseAmountByCategory = 0;

                        // Iterate over every record in the raw JSON response from the API
                        expenseRecords.forEach(expenseRecord => {
                            if (expenseRecord.category === expenseCategory) {
                                expenseAmountByCategory = expenseAmountByCategory + parseInt(expenseRecord.amount)
                            }
                        })
                        expenseAmounts.push(expenseAmountByCategory)
                        expenseColors.push(randomColor())
                    })
                    setExpenseLabels(expenseCategories)
                    setExpenses(expenseAmounts)
                    setExpenseBgColors(expenseColors);
                }
            })
    }, [])

    const expenseData = {
        labels: expenseLabels,
        datasets: [{
            label: "Expense Records",
            data: expenses,
            backgroundColor: "rgba(194, 116, 161, 0.5)",
            borderColor: "rgb(194, 116, 161)"
        }]
    }

    return (
        <React.Fragment>
            <Form onSubmit={(e) => filterByDate(e)} className="mb-4">
                <Row>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="startDate">
                            <Form.Label>Start Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="Start Date"
                                value={startDate}
                                onChange={e => setStartDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="endDate">
                            <Form.Label>End Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="End Date"
                                value={endDate}
                                onChange={e => setEndDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                </Row>
                {/* Conditionally render submit button based on isActive state */}
                {isActive
                    ?
                    <Button
                    variant="outline-primary"
                    type="submit"
                    id="submitBtn"
                    className="w-100"
                    >
                        Submit
                    	</Button>
                    :
                    <Button
                    variant="outline-primary"
                    type="submit"
                    id="submitBtn"
                    className="w-100"
                        disabled
                    >
                        Submit
                    	</Button>
                }
            </Form>
            <Radar data={expenseData} />
        </React.Fragment >
    )
}