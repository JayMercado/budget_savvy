import React from 'react';
import { Tabs, Tab, Row, Col } from 'react-bootstrap';
import RadarChartIncome from './radarchart-income';
import RadarChartExpense from './radarchart-expense';

export default function Index() {
    return (
        <Tabs defaultActiveKey="income" id="linecharts">
            <Tab eventKey="income" title="Income">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <RadarChartIncome />
                    </Col>
                </Row>
            </Tab>
            <Tab eventKey="expense" title="Expense">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <RadarChartExpense />
                    </Col>
                </Row>
            </Tab>
        </Tabs>
    )
}