import React, { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import moment from 'moment';
import { Pie } from 'react-chartjs-2';
import { Form, Button, Row, Col } from 'react-bootstrap';
import randomColor from 'randomcolor';


export default function PieChartIncome() {

    // States for dates
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [isActive, setIsActive] = useState(false);

    // State to store the transactions details of the user
    const [incomeLabels, setIncomeLabels] = useState([]);
    const [incomes, setIncomes] = useState([]);
    const [incomeBgColors, setIncomeBgColors] = useState([]);

    // Change format of date from form
    const firstDay = moment(startDate).format();
    const lastDay = moment(endDate).format();

    function filterByDate(e) {

        e.preventDefault()

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                /*
                *
                Filter Income Records
                *
                */

                // Variable to store all income records
                const newIncomeRecords = [];
                // Loop through all records
                data.records.map(record => {
                    // Push only if record type is income
                    if (record.type === "income") {
                        newIncomeRecords.push(record)
                    }
                })

                // Variables to temporarily store filtered income records
                let filteredIncomeRecords = [];

                if (newIncomeRecords.length > 0) {
                    // Loop through all income records
                    newIncomeRecords.map(newIncomeRecord => {

                        // Change format of each income date to match date format of the firstDay and lastDay
                        const newIncomeDay = moment(newIncomeRecord.date).format();

                        // Check if incomeDay is between firstday and lastDay
                        if (moment(newIncomeDay).isSameOrAfter(firstDay, "day") && moment(newIncomeDay).isSameOrBefore(lastDay, "day")) {
                            // Push Income Data
                            filteredIncomeRecords.push(newIncomeRecord)
                        }
                    })
                }

                // Variable to temporarily store the distinct income categories
                let filteredIncomeCategories = [];
                // Loop through filtered income records
                filteredIncomeRecords.forEach(filteredIncomeRecord => {
                    // Filter duplicates income categories
                    if (!filteredIncomeCategories.find(filteredIncomeCategory => filteredIncomeCategory === filteredIncomeRecord.category)) {
                        // Push inside temporary array only the distinct category
                        filteredIncomeCategories.push(filteredIncomeRecord.category);
                    }
                })

                // Variables to temporarily store filtered income data
                let filteredIncomeColors = [];
                let filteredIncomeAmounts = [];

                // Iterate over the distinct income categories
                filteredIncomeCategories.map(filteredIncomeCategory => {
                    // Variable to temporarily store total amount of each income category
                    let filteredAmountByCategory = 0;

                    // Iterate over every record in the raw JSON response from the API
                    filteredIncomeRecords.forEach(filteredIncomeRecord => {
                        if (filteredIncomeRecord.category === filteredIncomeCategory) {
                            filteredAmountByCategory = filteredAmountByCategory + parseInt(filteredIncomeRecord.amount)
                        }
                    })
                    filteredIncomeAmounts.push(filteredAmountByCategory)
                    filteredIncomeColors.push(randomColor())
                })

                setIncomeLabels(filteredIncomeCategories)
                setIncomes(filteredIncomeAmounts)
                setIncomeBgColors(filteredIncomeColors);
            })
    }

    // Validate form input whenever date fields are populated
    useEffect(() => {

        if (startDate && endDate) {
            setIsActive(true)
        }

    }, [startDate, endDate])

    useEffect(() => {
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data._id) {
                    /*
                    *
                    Income Records
                    *
                    */

                    // Variable to temporarily store income records
                    const incomeRecords = [];

                    // Loop through all records
                    data.records.map(record => {
                        // Push only if record type is income
                        if (record.type === "income") {
                            incomeRecords.push(record)
                        }
                    })

                    // Variable to temporarily store the distinct income categories
                    let incomeCategories = [];
                    // Loop through all income records
                    incomeRecords.forEach(incomeRecord => {
                        // Filter duplicates income categories
                        if (!incomeCategories.find(incomeCategory => incomeCategory === incomeRecord.category)) {
                            // Push inside temporary array only the distinct category
                            incomeCategories.push(incomeRecord.category);
                        }
                    })

                    // Variables to temporarily store income data
                    let incomeColors = [];
                    let incomeAmounts = [];

                    // Iterate over the distinct income categories
                    incomeCategories.map(incomeCategory => {
                        // Variable to temporarily store total amount of each income category
                        let amountByCategory = 0;

                        // Iterate over every record in the raw JSON response from the API
                        incomeRecords.forEach(incomeRecord => {
                            if (incomeRecord.category === incomeCategory) {
                                amountByCategory = amountByCategory + parseInt(incomeRecord.amount)
                            }
                        })
                        incomeAmounts.push(amountByCategory)
                        incomeColors.push(randomColor())
                    })
                    setIncomeLabels(incomeCategories)
                    setIncomes(incomeAmounts)
                    setIncomeBgColors(incomeColors);
                }
            })
    }, [])

    const incomeData = {
        labels: incomeLabels,
        datasets: [{
            data: incomes,
            backgroundColor: incomeBgColors,
            hoverBackground: incomeBgColors
        }]
    }

    return (
        <React.Fragment>
            <Form onSubmit={(e) => filterByDate(e)}  className="mb-4">
                <Row>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="startDate">
                            <Form.Label>Start Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="Start Date"
                                value={startDate}
                                onChange={e => setStartDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="endDate">
                            <Form.Label>End Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="End Date"
                                value={endDate}
                                onChange={e => setEndDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                </Row>
                {/* Conditionally render submit button based on isActive state */}
                {isActive
                    ?
                    <Button
                        variant="outline-primary"
                        type="submit"
                        id="submitBtn"
                        className="w-100"
                    >
                        Submit
                    	</Button>
                    :
                    <Button
                        variant="outline-primary"
                        type="submit"
                        id="submitBtn"
                        className="w-100"
                        disabled
                    >
                        Submit
                    	</Button>
                }
            </Form>
            <Pie data={incomeData} />
        </React.Fragment >
    )
}