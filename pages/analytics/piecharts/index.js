import React from 'react';
import { Tabs, Tab, Col, Row } from 'react-bootstrap';
import PieChartIncome from './piechart-income';
import PieChartExpense from './piechart-expense';

export default function Index() {
    return (
        <Tabs defaultActiveKey="income" id="linecharts">
            <Tab eventKey="income" title="Income">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <PieChartIncome />
                    </Col>
                </Row>
            </Tab>
            <Tab eventKey="expense" title="Expense">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <PieChartExpense />
                    </Col>
                </Row>
            </Tab>
        </Tabs>
    )
}