import React from 'react';
import Head from 'next/head';
import { Tabs, Tab } from 'react-bootstrap';
import PieCharts from './piecharts';
import LineCharts from './linecharts';
import BarCharts from './barcharts';
import RadarCharts from './radarcharts';

export default function Index() {
    return (
        <React.Fragment>
            <Head>
                <title>Analytics</title>
            </Head>
            <Tabs defaultActiveKey="pieChart" id="uncontrolled-tab-example" className="mt-3">
                <Tab eventKey="pieChart" title="Pie Chart">
                    <PieCharts />
                </Tab>
                <Tab eventKey="lineChart" title="Line Chart">
                    <LineCharts />
                </Tab>
                <Tab eventKey="barChart" title="Bar Chart">
                    <BarCharts />
                </Tab>
                <Tab eventKey="radarChart" title="Radar Chart">
                    <RadarCharts />
                </Tab>
            </Tabs>
        </React.Fragment>

    )
}