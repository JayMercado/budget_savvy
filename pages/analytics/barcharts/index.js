import React from 'react';
import { Tabs, Tab, Row,  Col } from 'react-bootstrap';
import BarChartTrend from './barchart-trend';
import BarChartIncome from './barchart-income';
import BarChartExpense from './barchart-expense';

export default function Index() {
    return (
        <Tabs defaultActiveKey="trend" id="linecharts">
            <Tab eventKey="trend" title="Trend">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <BarChartTrend />
                    </Col>
                </Row>
            </Tab>
            <Tab eventKey="income" title="Income">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <BarChartIncome />
                    </Col>
                </Row>
            </Tab>
            <Tab eventKey="expense" title="Expense">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <BarChartExpense />
                    </Col>
                </Row>
            </Tab>
        </Tabs>
    )
}