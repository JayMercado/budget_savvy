import React, { useState, useEffect } from 'react';
import { Bar } from "react-chartjs-2";
import { Form, Button, Row, Col } from 'react-bootstrap';
import moment from 'moment';

export default function BarChartIncome() {

    // States for dates
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [isActive, setIsActive] = useState(false);

    // State to store the transactions details of the user
    const [incomeLabels, setIncomeLabels] = useState([]);
    const [incomes, setIncomes] = useState([]);

    // Change format of date from form
    const firstDay = moment(startDate).format();
    const lastDay = moment(endDate).format();

    function filterByDate(e) {

        e.preventDefault()

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                /*
                *
                Filter Income Records
                *
                */

                // Variable to store all income records
                const newIncomeRecords = [];

                data.records.map(record => {
                    if (record.type === "income") {
                        newIncomeRecords.push(record)
                    }
                })

                // Variables to temporarily store filtered income records
                let incomeNewDates = [];
                let incomeNewAmounts = [];

                if (newIncomeRecords.length > 0) {
                    // Loop through all income records
                    newIncomeRecords.map(income => {

                        // Change format of each income date to match date format of the firstDay and lastDay
                        const incomeDay = moment(income.date).format();

                        // Check if incomeDay is between firstday and lastDay
                        if (moment(incomeDay).isSameOrAfter(firstDay, "day") && moment(incomeDay).isSameOrBefore(lastDay, "day")) {
                            // Puch Income Data
                            incomeNewDates.push(moment(income.date).format('MMM Do YY'))
                            incomeNewAmounts.push(income.amount)
                        }
                    })
                }
                // Change states of income data to new filtered income data
                setIncomeLabels(incomeNewDates)
                setIncomes(incomeNewAmounts)
            })
    }

    // Validate form input whenever date fields are populated
    useEffect(() => {

        if (startDate && endDate) {
            setIsActive(true)
        }

    }, [startDate, endDate])

    useEffect(() => {
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                /*
                *
                Income Records Data
                *
                */

                // Store income records
                const incomeRecords = [];

                // Loop through all records
                data.records.map(record => {
                    // Push only if record type is income
                    if (record.type === "income") {
                        incomeRecords.push(record)
                    }
                })

                // Variables to temporarily store income data
                let incomeDates = [];
                let incomeAmounts = [];

                if (incomeRecords.length > 0) {
                    // Loop through all income records
                    incomeRecords.forEach(income => {
                        // Push Income Data
                        incomeDates.push(moment(income.date).format('MMM Do YY'))
                        incomeAmounts.push(income.amount)
                    })
                    // Change state to contain income record data
                    setIncomeLabels(incomeDates)
                    setIncomes(incomeAmounts);
                }

            })
    }, [])

    const incomeData = {
        labels: incomeLabels,
        datasets: [
            {
                label: "Income",
                data: incomes,
                fill: true,
                backgroundColor: "rgba(200, 247, 197, 0.5)",
                borderColor: "rgba(102, 204, 153, 1)",
                hoverBackgroundColor: 'rgba(200, 247, 197, 1)',
                hoverBorderColor: 'rgba(102, 204, 153, 1)',
                borderWidth: 1,
            }
        ]
    };

    return (
        <React.Fragment>
            <Form onSubmit={(e) => filterByDate(e)} className="mb-4">
                <Row>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="startDate">
                            <Form.Label>Start Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="Start Date"
                                value={startDate}
                                onChange={e => setStartDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="endDate">
                            <Form.Label>End Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="End Date"
                                value={endDate}
                                onChange={e => setEndDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                </Row>
                {/* Conditionally render submit button based on isActive state */}
                {isActive
                    ?
                    <Button
                        variant="outline-primary"
                        type="submit"
                        id="submitBtn"
                        className="w-100"
                    >
                        Submit
                    	</Button>
                    :
                    <Button
                        variant="outline-primary"
                        type="submit"
                        id="submitBtn"
                        className="w-100"
                        disabled
                    >
                        Submit
                    	</Button>
                }
            </Form>
            <Bar data={incomeData} />
        </React.Fragment>

    );
}