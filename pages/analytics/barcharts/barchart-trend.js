import React, { useState, useEffect } from 'react';
import { Bar } from "react-chartjs-2";
import { Form, Button, Row, Col, } from 'react-bootstrap';
import moment from 'moment';

export default function BarChartTrend() {

    // States for dates
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [isActive, setIsActive] = useState(false);

    // State to store the transactions details of the user
    const [recordLabels, setRecordLabels] = useState([]);
    const [records, setRecords] = useState([]);

    // Change format of date from form
    const firstDay = moment(startDate).format();
    const lastDay = moment(endDate).format();

    function filterByDate(e) {

        e.preventDefault()

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                /*
                *
                Filter All Records
                *
                */

                // Variable to store filtered balance
                let filteredBalance = 0;

                // Variables to temporarily store filtered record data
                let filteredRecordDates = [];
                let filteredRecordAmounts = [];

                // Loop through all records
                data.records.forEach(record => {
                    // Change format of each record date to match date format of the firstDay and lastDay
                    const recordDay = moment(record.date).format();

                    // Check if recordDay is between firstday and lastDay
                    if (moment(recordDay).isSameOrAfter(firstDay, "day") && moment(recordDay).isSameOrBefore(lastDay, "day")) {
                        // if the type of the record is "income" add the amount of the record to the balance
                        if (record.type === "income") {
                            filteredBalance += record.amount
                        }
                        // if the type of the record is "expense" subtract the amount of the record from the balance
                        else if (record.type === "expense") {
                            filteredBalance -= record.amount
                        }
                        filteredRecordDates.push(moment(record.date).format('MMM Do YY'))
                        filteredRecordAmounts.push(filteredBalance)

                    }
                })
                // Change states of record data to new filtered record data
                setRecordLabels(filteredRecordDates)
                setRecords(filteredRecordAmounts);

            })
    }

    // Validate form input whenever date fields are populated
    useEffect(() => {

        if (startDate && endDate) {
            setIsActive(true)
        }

    }, [startDate, endDate])

    useEffect(() => {
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                /*
                *
                All Records Data
                *
                */

                // Variable to store balance
                let balance = 0;

                // Variables to temporarily store record data
                let recordDates = [];
                let recordAmounts = [];

                // Loop through all records
                data.records.forEach(record => {
                    // if the type of the record is "income" add the amount of the record to the balance
                    if (record.type === "income") {
                        balance += record.amount
                    }
                    // if the type of the record is "expense" subtract the amount of the record from the balance
                    else if (record.type === "expense") {
                        balance -= record.amount
                    }
                    recordDates.push(moment(record.date).format('MMM Do YY'))
                    recordAmounts.push(balance)
                })
                // Change states to contain record data
                setRecordLabels(recordDates)
                setRecords(recordAmounts);

            })
    }, [])

    const recordData = {
        labels: recordLabels,
        datasets: [
            {
                label: "Balance",
                data: records,
                backgroundColor: "rgba(197, 239, 247, 0.5)",
                borderColor: "rgba(129, 207, 224, 1)",
                hoverBackgroundColor: 'rgba(197, 239, 247, 1)',
                hoverBorderColor: 'rgba(129, 207, 224, 1)',
                borderWidth: 1,
            }
        ]
    };

    return (
        <React.Fragment>
            <Form onSubmit={(e) => filterByDate(e)} className="mb-4">
                <Row>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="startDate">
                            <Form.Label>Start Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="Start Date"
                                value={startDate}
                                onChange={e => setStartDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="endDate">
                            <Form.Label>End Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="End Date"
                                value={endDate}
                                onChange={e => setEndDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                </Row>
                {/* Conditionally render submit button based on isActive state */}
                {isActive
                    ?
                    <Button
                        variant="outline-primary"
                        type="submit"
                        id="submitBtn"
                        className="w-100"
                    >
                        Submit
                    	</Button>
                    :
                    <Button
                        variant="outline-primary"
                        type="submit"
                        id="submitBtn"
                        className="w-100"
                        disabled
                    >
                        Submit
                    	</Button>
                }
            </Form>
            <Bar data={recordData} />
        </React.Fragment>

    );
}