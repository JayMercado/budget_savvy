import React, { useState, useEffect } from 'react';
import { Bar } from "react-chartjs-2";
import { Form, Button, Row, Col } from 'react-bootstrap';
import moment from 'moment';

export default function BarChartExpense() {

    // States for dates
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [isActive, setIsActive] = useState(false);

    // State to store the transactions details of the user
    const [expenseLabels, setExpenseLabels] = useState([]);
    const [expenses, setExpenses] = useState([]);

    // Change format of date from form
    const firstDay = moment(startDate).format();
    const lastDay = moment(endDate).format();

    function filterByDate(e) {

        e.preventDefault()

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                /*
                *
                Filter Expense Records
                *
                */

                // Variable to store all expense records
                const newExpenseRecords = [];

                data.records.forEach(record => {
                    if (record.type === "expense") {
                        newExpenseRecords.push(record)
                    }
                })

                // Variables to temporarily store filtered expense records
                let expenseNewDates = [];
                let expenseNewAmounts = [];

                if (newExpenseRecords.length > 0) {
                    // Loop through all expense records
                    newExpenseRecords.map(expense => {

                        // Change format of each expense date to match date format of the firstDay and lastDay
                        const expenseDay = moment(expense.date).format();

                        // Check if expenseDay is between firstday and lastDay
                        if (moment(expenseDay).isSameOrAfter(firstDay, "day") && moment(expenseDay).isSameOrBefore(lastDay, "day")) {
                            // Push expense data
                            expenseNewDates.push(moment(expense.date).format('MMM Do YY'))
                            expenseNewAmounts.push(expense.amount)
                        }
                    })
                }
                // Change states of expense data to new filtered expense data
                setExpenseLabels(expenseNewDates)
                setExpenses(expenseNewAmounts)
            })
    }

    // Validate form input whenever date fields are populated
    useEffect(() => {

        if (startDate && endDate) {
            setIsActive(true)
        }

    }, [startDate, endDate])

    useEffect(() => {
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                /*
                *
                Expense Records Data
                *
                */

                // Store expense records
                const expenseRecords = [];

                // Loop through all records
                data.records.forEach(record => {
                    // Push only if record type is expense
                    if (record.type === "expense") {
                        expenseRecords.push(record)
                    }
                })

                // Variables to temporarily store income data
                let expenseDates = [];
                let expenseAmounts = [];

                if (expenseRecords.length > 0) {
                    // Loop through all income records
                    expenseRecords.forEach(expense => {
                        // Push Expense Data
                        expenseDates.push(moment(expense.date).format('MMM Do YY'))
                        expenseAmounts.push(expense.amount)
                    })
                    // Change state to contain expense record data
                    setExpenseLabels(expenseDates)
                    setExpenses(expenseAmounts);
                }

            })
    }, [])

    const expenseData = {
        labels: expenseLabels,
        datasets: [
            {
                label: "Expenses",
                data: expenses,
                fill: true,
                backgroundColor: "rgba(241, 169, 160, 0.5)",
                borderColor: "rgba(224, 130, 131, 1)",
                hoverBackgroundColor: 'rgba(241, 169, 160, 1)',
                hoverBorderColor: 'rgba(224, 130, 131, 1)',
                borderWidth: 1,
            }
        ]
    };

    return (
        <React.Fragment>
            <Form onSubmit={(e) => filterByDate(e)} className="mb-4">
                <Row>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="startDate">
                            <Form.Label>Start Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="Start Date"
                                value={startDate}
                                onChange={e => setStartDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                    <Col xs="12" lg="6">
                        <Form.Group controlId="endDate">
                            <Form.Label>End Date</Form.Label>
                            <Form.Control
                                type="date"
                                placeholder="End Date"
                                value={endDate}
                                onChange={e => setEndDate(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Col>
                </Row>
                {/* Conditionally render submit button based on isActive state */}
                {isActive
                    ?
                    <Button
                        variant="outline-primary"
                        type="submit"
                        id="submitBtn"
                        className="w-100"
                    >
                        Submit
                    	</Button>
                    :
                    <Button
                        variant="outline-primary"
                        type="submit"
                        id="submitBtn"
                        className="w-100"
                        disabled
                    >
                        Submit
                    	</Button>
                }
            </Form>
            <Bar data={expenseData} />
        </React.Fragment>

    );
}