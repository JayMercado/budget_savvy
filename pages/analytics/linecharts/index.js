import React from 'react';
import { Tabs, Tab, Row, Col } from 'react-bootstrap';
import LineChartTrend from './linechart-trend';
import LineChartIncome from './linechart-income';
import LineChartExpense from './linechart-expense';

export default function Index() {
    return (
        <Tabs defaultActiveKey="trend" id="linecharts">
            <Tab eventKey="trend" title="Trend">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <LineChartTrend />
                    </Col>
                </Row>
            </Tab>
            <Tab eventKey="income" title="Income">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <LineChartIncome />
                    </Col>
                </Row>
            </Tab>
            <Tab eventKey="expense" title="Expense">
                <Row className="justify-content-center mt-2">
                    <Col xs md="9">
                        <LineChartExpense />
                    </Col>
                </Row>
            </Tab>
        </Tabs>
    )
}