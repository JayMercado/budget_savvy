import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import Router from 'next/router';
import UserContext from '../UserContext';
import View from '../components/View';
import Swal from 'sweetalert2';

export default function Register() {

    return (
        <View title={'Register'}>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Register</h3>
                    <RegisterUsser />
                </Col>
            </Row>
        </View>
    )
}

const RegisterUsser = () => {

    // Form input state hooks
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const { user, setUser } = useContext(UserContext);

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // Validate form input whenever email, password1, or password2 is changed
    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if (email !== '' && firstName !== '' && lastName !== '' && (password1 !== '' && password2 !== '') && (password2 === password1)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [email, password1, password2, firstName, lastName])

    // Function to register user
    function registerUser(e) {

        e.preventDefault();

        // Check for duplicate email in database first
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/email-exists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
            .then(res => res.json())
            .then(data => {

                // If no duplicates found
                if (data === false) {

                    fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/register`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            password: password1
                        })
                    })
                        .then(res => res.json())
                        .then(data => {

                            // Registration successful
                            if (data === true) {
                                // Login
                                const options = {
                                    method: 'POST',
                                    headers: { 'Content-Type': 'application/json' },
                                    body: JSON.stringify({
                                        email: email,
                                        password: password1
                                    })
                                }

                                fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/login`, options)
                                    .then(res => res.json())
                                    .then(data => {
                                        if (typeof data.accessToken !== 'undefined') {
                                            localStorage.setItem('token', data.accessToken)
                                            retrieveUserDetails(data.accessToken)
                                            Router.push('/profile')
                                        } else {
                                            if (data.error === 'does-not-exist') {
                                                Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                                            } else if (data.error === 'login-type-error') {
                                                Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try an alternative login procedure.', 'error')
                                            } else if (data.error === 'incorrect-password') {
                                                Swal.fire('Authentication Failed', 'Password is incorrect.', 'error');
                                            }
                                        }

                                    })




                            } else {
                                // Error in creating registration, redirect to error page
                                Router.push('/error')
                            }

                        })

                } else { // Duplicate email found
                    Swal.fire(
                        'Email Already Registered',
                        'The email you want to register has already been used.',
                        'error'
                    )
                }

            })
    }

    const retrieveUserDetails = (accessToken) => {

        const options = {
            headers: { Authorization: `Bearer ${accessToken}` }
        };

        // localhost:4000/api/users/details
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, options)
            .then(res => res.json())
            .then(data => {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            })

    }

    return (
        <React.Fragment>
            <Card>
                <Card.Header>Register Account</Card.Header>
                <Card.Body>
                    <Form onSubmit={(e) => registerUser(e)}>

                        <Form.Group controlId="firstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="First Name"
                                value={firstName}
                                onChange={e => setFirstName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="lastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Last Name"
                                value={lastName}
                                onChange={e => setLastName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                    </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="password1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password1}
                                onChange={e => setPassword1(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password2">
                            <Form.Label>Verify Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Verify Password"
                                value={password2}
                                onChange={e => setPassword2(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Conditionally render submit button based on isActive state */}
                        {isActive
                            ?
                            <div className="text-center">
                                <Button
                                    variant="outline-primary"
                                    className="w-100"
                                    type="submit"
                                    id="submitBtn"
                                >
                                    Submit
                    	</Button>
                            </div>
                            :
                            <div className="text-center">
                                <Button
                                    variant="outline-danger"
                                    className="w-100"
                                    type="submit"
                                    id="submitBtn"
                                    disabled
                                >
                                    Submit
                    	</Button>
                            </div>
                        }

                    </Form>
                </Card.Body>
            </Card>
        </React.Fragment>
    )
}