import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import '../styles/globals.css';
// Bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';
import { UserProvider } from '../UserContext';
import Head from 'next/head';
import Navbar from '../components/NavBar';

export default function App({ Component, pageProps }) {

    //State hook for user state, define here for global scope 
    const [user, setUser] = useState({
        // Initialized as an object with properties set as null
        // Proper values will be obtained from localStorage AFTER component gets rendered due to Next.JS pre-rendering
        id: null
    })

    // Effect hook to set global user state when changes to the id property of user state is detected
    useEffect(() => {

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                if (data._id) { //JWT validated
                    setUser({
                        id: data._id
                    })
                } else { //JWT is invalid or non-existent
                    setUser({
                        id: null
                    })
                }

            })

    }, [user.id])

    // Function for clearing local storage upon logout
    const unsetUser = () => {

        localStorage.clear()

        // Set the user global scope in the context provider to have its id set to null
        setUser({
            id: null
        });

    }

    return (
        <React.Fragment>
            {/* Wrap the component tree within the UserProvider context provider so that components will have access to the passed in values here */}
            <UserProvider value={{ user, setUser, unsetUser }}>
                <Head>
                <link rel="icon" href="/images/savvy_icon.svg" />
                </Head>
                
                <Container style={{marginBottom: "100px"}}>
                    <Navbar />
                    <Component {...pageProps} />
                </Container>
                <footer className="fixed-bottom text-center mt-5 mb-2">&copy; 2021 built by Jay Mercado | Zuitt Coding Bootcamp</footer>
            </UserProvider>
        </React.Fragment>
    )
}