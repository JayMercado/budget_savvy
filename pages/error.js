import React from 'react';
import Head from 'next/head';
import Banner from '../components/Banner';

export default function Error() {

	const data = {
		title: "Something went wrong",
		content: "Please try again later."
	}

	return (
		<React.Fragment>
			<Head>
				<title>Oops...</title>
			</Head>
			<div className="my-5">
			<Banner
				data={data}
			/>
			</div>
		</React.Fragment>
	)
}