import React, { useState, useEffect } from 'react';
import { Form, Button, Container, Row, Col, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';
import AppHelper from '../../app-helper';

export default function addIncome() {

    // Form input state hooks
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [amount, setAmount] = useState(0);

    // State hook for list of income categories
    const [listIncomeCategories, setListIncomeCategories] = useState([]);

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // Validate form input when all fields are populated
    useEffect(() => {

        // Validation to enable submit button when all fields are populated
        if (title !== '' && description !== '' && amount !== '') {
            setIsActive(true)
        }

    }, [title, description, category, amount])

    // Retrieve all category names
    useEffect(() => {
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
        })
            .then(res => res.json())
            .then(data => {
                setListIncomeCategories(data.incomeCategories)
            })
    }, [])

    // Store in an array all categories names
    const list = listIncomeCategories.map(category => {
        return (
            <option key={category.name}>{category.name}</option>
        )
    })

    function addNewIncome(e) {

        //prevent redirection via form submission
        e.preventDefault();

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/add-record`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: title,
                description: description,
                type: "income",
                category: category,
                amount: amount
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    Swal.fire('Good Job!', 'You have created a new income record. Please, refresh the page.', 'success')
                } else {
                    // Error in creating record, redirect to error page
                    Router.push('/error')
                }
            })
    }

    return (
        <React.Fragment>
            <Container className="mt-5 mb-5">
                <Row className="justify-content-center">
                    <Col xs md="6">
                        <Card>
                            <Card.Header><h2 style={{color: "#131313"}}><b>New Income Category</b></h2></Card.Header>
                            <Card.Body>
                                <Form onSubmit={(e) => addNewIncome(e)}>

                                    <Form.Group controlId="incomeTitle">
                                        <Form.Label>Title</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Title"
                                            value={title}
                                            onChange={e => setTitle(e.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="incomeDescription">
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Description"
                                            value={description}
                                            onChange={e => setDescription(e.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="incomeCategory">
                                        <Form.Label>Income Category</Form.Label>
                                        <Form.Control
                                            as="select"
                                            value={category}
                                            onChange={e => setCategory(e.target.value)}
                                            required
                                        >
                                            {list}
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="incomeAmount">
                                        <Form.Label>Amount</Form.Label>
                                        <Form.Control
                                            type="number"
                                            placeholder="Amount"
                                            value={amount}
                                            onChange={e => setAmount(e.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    {/* Conditionally render submit button based on isActive state */}
                                    {isActive
                                        ?
                                        <Button
                                            variant="outline-primary"
                                            type="submit"
                                            id="incomeSubmitBtn"
                                            className="w-100"
                                        >
                                            Submit
                    	</Button>
                                        :
                                        <Button
                                            variant="outline-primary"
                                            type="submit"
                                            id="disabledIncomeSubmitBtn"
                                            className="w-100"
                                            disabled
                                        >
                                            Submit
                    	</Button>
                                    }

                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </React.Fragment>
    )
}