import React, { useState, useEffect, useContext } from 'react';
import SearchField from "react-search-field";
import moment from 'moment';
import { Table, Tabs, Tab } from 'react-bootstrap';

export default function Search() {

  // States for input form
  const [inputIncome, setInputIncome] = useState([]);
  const [inputExpense, setInputExpense] = useState([]);

  // State to contain list of records got from Database
  const [incomes, setIncomes] = useState([]);
  const [expenses, setExpenses] = useState([]);

  // State hooks to contain filtered results from search
  const [filteredIncomes, setFilteredIncomes] = useState([]);
  const [filteredExpenses, setFilteredExpenses] = useState([]);

  const onChangeIncome = (inputIncome) => {
    const filteredInc = incomes.filter(income => {
      return income.title.toLowerCase().includes(inputIncome.toLowerCase())
    })
    setInputIncome(inputIncome);
    setFilteredIncomes(filteredInc);
  }

  const onChangeExpense = (inputExpense) => {
    const filteredExp = expenses.filter(expense => {
      return expense.title.toLowerCase().includes(inputExpense.toLowerCase())
    })
    setInputExpense(inputExpense);
    setFilteredExpenses(filteredExp);
  }

  useEffect(() => {
    fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        const incomeRecords = [];

        data.records.map(record => {
          if (record.type === "income") {
            incomeRecords.push(record)
          }
        })

        const expenseRecords = [];

        data.records.forEach(record => {
          if (record.type === "expense") {
            expenseRecords.push(record)
          }
        })

        setIncomes(incomeRecords);
        setExpenses(expenseRecords);
      })
  }, [])

  // Store in an array all filtered results of income search
  const listIncome = filteredIncomes.map(filteredIncome => {
    return (
      <tr key={filteredIncome._id}>
        <td>{filteredIncome.title}</td>
        <td>{filteredIncome.description}</td>
        <td>{moment(filteredIncome.date).format('MMMM DD YYYY')}</td>
        <td>{filteredIncome.category}</td>
        <td>{filteredIncome.amount}</td>
      </tr>
    )
  })

  // Store in an array all filtered results of expense search
  const listExpense = filteredExpenses.map(filteredExpense => {
    return (
      <tr key={filteredExpense._id}>
        <td>{filteredExpense.title}</td>
        <td>{filteredExpense.description}</td>
        <td>{moment(filteredExpense.date).format('MMMM DD YYYY')}</td>
        <td>{filteredExpense.category}</td>
        <td>{filteredExpense.amount}</td>
      </tr>
    )
  })

  return (
    <React.Fragment>
      <Tabs defaultActiveKey="searchIncome" id="searchRecord">
        <Tab eventKey="searchIncome" title="Search Income Record">
          <div>
            <h2 style={{ color: "#131313" }}><b>Search Income</b></h2>
            <SearchField
              placeholder="Search..."
              onChange={onChangeIncome}
              classNames="search-income"
            />
            <Table striped bordered hover>
              <tbody>
                {listIncome}
              </tbody>
            </Table>
          </div>
        </Tab>
        <Tab eventKey="searchExpense" title="Search Expense Record">
          <div>
            <h2 style={{ color: "#131313" }}><b>Search Expenses</b></h2>
            <SearchField
              placeholder="Search..."
              onChange={onChangeExpense}
              classNames="search-expense"
            />
            <Table striped bordered hover>
              <tbody>
                {listExpense}
              </tbody>
            </Table>
          </div>
        </Tab>
      </Tabs>
    </React.Fragment>
  )
}