import React, { useState, useEffect, useContext } from 'react';
import { Table, Alert, Tabs, Tab } from 'react-bootstrap';
import moment from 'moment';

export default function History() {

    // State to store the transactions details of the user.
    const [incomes, setIncomes] = useState([]);
    const [expenses, setExpenses] = useState([]);

    // "useEffect()" hook to retrieve the transaction details of the user from the database and store the records in the records state.
    useEffect(() => {
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                const incomeRecords = [];

                data.records.map(record => {
                    if (record.type === "income") {
                        incomeRecords.push(record)
                    }
                })

                const expenseRecords = [];

                data.records.forEach(record => {
                    if (record.type === "expense") {
                        expenseRecords.push(record)
                    }
                })

                setIncomes(incomeRecords);
                setExpenses(expenseRecords);
            })
    }, [])


    return (
        <React.Fragment>
            <Tabs defaultActiveKey="incomeRecords" id="uncontrolled-tab-example">
                <Tab eventKey="incomeRecords" title="Income Records">
                    {incomes.length > 0
                        ?
                        <Table striped bordered hover className="mt-3">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                    <th>Category</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                {incomes.map(income => {
                                    return (
                                        <tr key={income._id}>
                                            <td>{income.title}</td>
                                            <td>{income.description}</td>
                                            <td>{moment(income.date).format('MMMM DD YYYY')}</td>
                                            <td>{income.category}</td>
                                            <td>{income.amount}</td>
                                        </tr>
                                    )
                                })
                                }
                            </tbody>
                        </Table>
                        :
                        <Alert variant="info">You have no income transactions yet.</Alert>
                    }
                </Tab>
                <Tab eventKey="expenseRecords" title="Expense Records">
                    {expenses.length > 0
                        ?
                        <Table striped bordered hover className="mt-3">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                    <th>Category</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                {expenses.map(expense => {
                                    return (
                                        <tr key={expense._id}>
                                            <td>{expense.title}</td>
                                            <td>{expense.description}</td>
                                            <td>{moment(expense.date).format('MMMM DD YYYY')}</td>
                                            <td>{expense.category}</td>
                                            <td>{expense.amount}</td>
                                        </tr>
                                    )
                                })
                                }
                            </tbody>
                        </Table>
                        :
                        <Alert variant="info">You have no expense transactions yet.</Alert>
                    }
                </Tab>
            </Tabs>
        </React.Fragment>
    )
}
