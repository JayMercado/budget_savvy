import React, { useState, useEffect } from 'react';
import { Form, Button, Container, Row, Col, Card } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function addIncomeCategory() {

    // Form input state hooks
    const [name, setName] = useState('')

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // Validate form input whenever email, password1, or password2 is changed
    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if (name !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [name])

    // Function to add new income category
    function addNewIncomeCategory(e) {

        e.preventDefault();

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                const incomeCategoriesNames = data.incomeCategories.map(incomeCategory => incomeCategory.name)
                if (incomeCategoriesNames.includes(name)) {
                    Swal.fire(
                        'Expense Category Already Exists',
                        'You already created this category. Choose another name.',
                        'error'
                    )
                } else {

                    fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/add-income-category`, {
                        method: 'POST',
                        headers: {
                            'Authorization': `Bearer ${localStorage.getItem('token')}`,
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            name: name
                        })
                    })
                        .then(res => res.json())
                        .then(data => {

                            // If successful
                            if (data === true) {
                                Swal.fire('Good Job!', 'You have created a new income category. Please, refresh the page.', 'success')
                            } else {
                                // Error in creating registration, redirect to error page
                                Router.push('/error')
                            }

                        })
                }
            })
    }

    return (
        <React.Fragment>
            <Container className="mt-5 mb-5">
                <Row className="justify-content-center">
                    <Col xs md="6">
                        <Card>
                            <Card.Header><h2 style={{color: "#131313"}}><b>New Expense Category</b></h2></Card.Header>
                            <Card.Body>
                                <Form onSubmit={(e) => addNewIncomeCategory(e)}>

                                    <Form.Group controlId="incomeCategoryName">
                                        <Form.Control
                                            type="text"
                                            placeholder="Income Category"
                                            value={name}
                                            onChange={e => setName(e.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    {/* Conditionally render submit button based on isActive state */}
                                    {isActive
                                        ?
                                        <Button
                                            variant="outline-primary"
                                            type="submit"
                                            id="incomeCategorySubmitBtn"
                                            className="w-100"
                                        >
                                            Submit
                    	                </Button>
                                        :
                                        <Button
                                            variant="outline-primary"
                                            type="submit"
                                            id="disabledIncomeCategorySubmitBtn"
                                            className="w-100"
                                            disabled
                                        >
                                            Submit
                    	                </Button>
                                    }

                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </React.Fragment >

    )
}