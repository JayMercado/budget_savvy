import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Tabs, Tab, Jumbotron, Button, Form, Alert } from 'react-bootstrap';
import History from './history';
import AddIncomeCategory from './add-income-category';
import AddExpenseCategory from './add-expense-category';
import AddIncome from './add-income';
import AddExpense from './add-expense';
import Search from './search';

export default function Index() {

    // State to store the transactions details of the user.
    const [incomes, setIncomes] = useState([]);
    const [expenses, setExpenses] = useState([]);
    // States to store first name and last name of the user
    const [forename, setForename] = useState('')
    const [surname, setSurame] = useState('')
    const [showEdit, setShowEdit] = useState(false)
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // Variable to store the balance of the user.
    let balance = 0;

    function updateUser(e) {

        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/update-user`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: forename,
                lastName: surname,
            })
        })
            .then(res => res.json())
            .then(data => {
            })
    }

    // Retrieve first name, last name and records to compute balance
    useEffect(() => {
        fetch(`https://sleepy-oasis-54215.herokuapp.com/api/users/details`, {
            headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
        })
            .then(res => res.json())
            .then(data => {
                setForename(data.firstName)
                setSurame(data.lastName)

                const incomeRecords = [];

                data.records.map(record => {
                    if (record.type === "income") {
                        incomeRecords.push(record)
                    }
                })

                const expenseRecords = [];

                data.records.forEach(record => {
                    if (record.type === "expense") {
                        expenseRecords.push(record)
                    }
                })

                setIncomes(incomeRecords);
                setExpenses(expenseRecords);
            })
    }, [])

    // Add all amounts in the income records to the balance
    incomes.forEach(income => {
        balance += income.amount
    })

    // Subtract all amounts in the expense records from the balance
    expenses.forEach(expense => {
        balance -= expense.amount
    })

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if (forename !== '' && surname !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [forename, surname])

    return (
        <React.Fragment>
            <Head>
                <title>Profile</title>
            </Head>
            <Tabs defaultActiveKey="profileTab" id="profile" className="mt-3">
                <Tab eventKey="profileTab" title="Profile">
                    {showEdit
                        ?
                        <Form onSubmit={updateUser}>
                            <Form.Group controlId="firstName">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="First Name"
                                    value={forename}
                                    onChange={(e) => setForename(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="lastName">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Last Name"
                                    value={surname}
                                    onChange={(e) => setSurame(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            <div className="text-center">
                                <Button variant="outline-info" className="mx-3" onClick={() => setShowEdit(false)}>Back</Button>
                                <Button variant="outline-primary" type="submit">Submit</Button>
                            </div>
                        </Form>
                        :
                        <Jumbotron className="my-5">
                            <img src="/images/savvy_profile.png" alt="Savvy Profile" className="mx-auto" style={{display: "block", width: "20%"}}/>
                            <h2 className="text-center">Hello, <b>{forename} {surname}</b>!</h2>
                            {balance >= 0
                                ?
                                <Alert variant="success">Your Balance is PHP{balance}</Alert>
                                :
                                <Alert variant="danger">Your Balance is PHP{balance}</Alert>
                            }
                            <p className="text-center my-5">“Never spend your money before you have it.” – Thomas Jefferson</p>
                            <div className="text-center">
                                <Button variant="outline-primary" className="text-center" onClick={() => setShowEdit(true)}>Edit Profile</Button>
                            </div>
                        </Jumbotron>
                    }
                </Tab>
                <Tab eventKey="history" title="History">
                    <History />
                </Tab>
                <Tab eventKey="category" title="Add Category">
                    <Tabs defaultActiveKey="addIncomeCategory" id="addRecordCategory">
                        <Tab eventKey="addIncomeCategory" title="Add Income Category">
                            <AddIncomeCategory />
                        </Tab>
                        <Tab eventKey="addExpenseCategory" title="Add Expense Category">
                            <AddExpenseCategory />
                        </Tab>
                    </Tabs>
                </Tab>
                <Tab eventKey="transaction" title="Add Transaction">
                    <Tabs defaultActiveKey="addIncomeRecord" id="addRecord">
                        <Tab eventKey="addIncomeRecord" title="Add Income Record">
                            <AddIncome />
                        </Tab>
                        <Tab eventKey="addExpenseRecord" title="Add Expense Record">
                            <AddExpense />
                        </Tab>
                    </Tabs>
                </Tab>
                <Tab eventKey="search" title="Search Transaction">
                    <Search />
                </Tab>
            </Tabs>
        </React.Fragment>

    )
}