import React from 'react';
import Head from 'next/head';
import styles from '../styles/Home.module.css';



export default function Home() {

	const data = {
		title: "Budget Savvy",
		content: "Track your transactions with an awesone budget tracker."
	}

	return (
		<React.Fragment>
			<Head>
				<title>Budget Savvy</title>
			</Head>
			<img src="/images/savvy_banner.png" alt="Savvy Banner" className="my-4" style={{width: "100%"}}/>
		</React.Fragment>
	)
}
